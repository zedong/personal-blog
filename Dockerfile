# Stage 1
FROM node:14.8.0-alpine3.12 as build-step

WORKDIR /usr/app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build --prod

# Stage 2
FROM nginx:1.19.1-alpine
COPY --from=build-step /usr/app/dist /app/usr/share/nginx/html